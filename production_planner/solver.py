from functools import lru_cache
from production_planner.models import Plant, Fuels, PlantTypes
from production_planner.exceptions import PowerAssignmentFailedError


def solver(asked_load: int, plants: list[Plant], fuels: Fuels, method: str = 'normal') -> list[float]:
    """
    Solves the production plan

    Args:
        asked_load: The load asked for the grid
        plants: The power plants available
        fuels: The fuels prices
        method: Method to use to process the production plan, 'normal' of 'recursive'

    Returns:

    """
    # Getting the plant prices
    prices = process_plant_prices(plants, fuels)
    # Getting the assignments by plant
    try:
        if method == 'normal':
            assignments = process_plant_assignments(
                prices, plants, asked_load, fuels
            )
        elif method == 'recursive':
            assignments = process_plant_assignment_recursive(
                prices, plants, asked_load, fuels
            )
        else:
            raise ValueError(f'Method {method} not supported to process the production plan')
    except PowerAssignmentFailedError:
        raise

    return assignments


def process_plant_prices(plants: list[Plant], fuels: Fuels) -> list[float]:
    prices = []

    for plant in plants:
        plant_price = 0
        # For Gas fired and turbojet plants, the bounds are the max power and min power
        if plant.type is PlantTypes.GAS_FIRED:
            plant_price = fuels.gas_price / plant.efficiency + 0.3 * fuels.co2_price
        elif plant.type is PlantTypes.TURBOJET:
            plant_price = fuels.kerosine_price / plant.efficiency

        prices.append(plant_price)

    return prices


def process_plant_assignments(prices: list[float], plants: list[Plant], asked_load: int, fuels: Fuels) -> list[float]:
    """
    Process the plants assignments by attributing the power to the lowest price plant first, and then the second one,
    etc... This method does not work in every case
    Args:
        prices: The prices by plant
        plants: The plants
        asked_load: The asked load to the plants
        fuels: The fuel prices

    Returns:
        list: The production plan for each plant

    """
    # Getting the id of plants ordered by prices
    id_sorted = sorted(range(len(prices)), key=lambda k: prices[k])
    assignments = [0, 0, 0, 0, 0, 0]

    # Associating power to the plants ordered by production price
    # This algorithm fails with the payload 2
    # First we need to find what values in the bounds are summing up to the ask load
    for i in id_sorted:
        plant = plants[i]
        p = 0
        if plant.type is PlantTypes.WIND_TURBINE:
            p = round(plant.pmax * fuels.wind_power / 100, ndigits=1)
        else:
            p = round(max(min(asked_load, plant.pmax), plant.pmin), ndigits=1)

        # If p is superior to the remaining asked load, we can't use this plant
        if p <= asked_load:
            assignments[i] = p
            asked_load -= p

    if asked_load != 0:
        raise PowerAssignmentFailedError('Could not find a production plan with the asked load and the power plant')

    return assignments


def process_plant_assignment_recursive(
        prices: list[float], plants: list[Plant], asked_load: int, fuels: Fuels) -> list[float]:
    """
    Process the plants assignments by using a recursive function in order to test almost all the possibilities.

    Args:
        prices: The prices by plant
        plants: The plants
        asked_load: The asked load to the plants
        fuels: The fuel prices

    Returns:
        list: The production plan for each plant

    """
    id_sorted = sorted(range(len(prices)), key=lambda k: prices[k])

    prices = [prices[i] for i in id_sorted]
    bounds = []
    for i in id_sorted:
        if plants[i].type is PlantTypes.WIND_TURBINE:
            power = round(plants[i].pmax * fuels.wind_power / 100)
            bound = (power, power)
        else:
            bound = (plants[i].pmin, plants[i].pmax)

        bounds.append(bound)

    best_combination = find_best_price(tuple(bounds), asked_load, tuple(prices), max(prices) * asked_load, 0)[0]

    assignments = [0 for p in plants]
    for i, j in enumerate(id_sorted):
        assignments[j] = best_combination[i]

    return assignments


def generator(bounds: list[int]):
    n = bounds[0]
    yield 0

    while n <= bounds[1]:
        yield n
        n += 1


@lru_cache(maxsize=None)
def find_best_price(bounds: tuple, asked_load: int, prices: tuple, price_to_beat: float, total_price: float):
    """
    This method recursively tries to find the production plan with the lowest price. But only with a precision of
    1 MWh instead of 0.1 MWh (too slow).

    Args:
        bounds: Bounds of each production plant
        asked_load: The load asked for the production plan
        prices: The prices by plant, must be in the same order as the bounds
        price_to_beat: The price to beat, initialised with the highest price * the asked load
        total_price: The total price in the recursion

    Returns:
        list: The assignments for each plant
        float: The total production price with the proposed plan

    """
    if asked_load == 0:
        return [0 for b in bounds], total_price
    elif len(bounds) == 1 and bounds[0][0] <= asked_load <= bounds[0][1]:
        return [asked_load], total_price + asked_load * prices[0]
    elif len(bounds) == 1:
        return False

    candidate = None

    for i in generator(bounds[0]):
        if i > asked_load:
            continue

        new_price = total_price + i * prices[0]

        if new_price > price_to_beat:
            break

        best_solution = find_best_price(bounds[1:], asked_load - i, prices[1:], price_to_beat, new_price)
        if best_solution is not False:
            if best_solution[1] < price_to_beat:
                candidate = [i] + best_solution[0]
                price_to_beat = best_solution[1]

    if candidate is not None:
        return candidate, price_to_beat

    return False
