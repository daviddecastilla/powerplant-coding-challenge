from enum import Enum
from pydantic import BaseModel, Field


class PlantTypes(Enum):
    GAS_FIRED = "gasfired"
    TURBOJET = "turbojet"
    WIND_TURBINE = "windturbine"


class Plant(BaseModel):
    name: str = Field(
        description="Name of the plant"
    )
    type: PlantTypes = Field(
        description="Type of the plant",
    )
    efficiency: float = Field(
        description="Plant efficiency"
    )
    pmin: int = Field(
        description="Minimum power deliverable by the plant"
    )
    pmax: int = Field(
        description="Maximum power deliverable by the plant"
    )


class Fuels(BaseModel):
    gas_price: float = Field(
        alias="gas(euro/MWh)",
        description="Price of a MWh of gas"
    )

    kerosine_price: float = Field(
        alias="kerosine(euro/MWh)",
        description="Price of a MWh of kerosine"
    )

    co2_price: float = Field(
        alias="co2(euro/ton)",
        description="Price of co2 emission"
    )

    wind_power: int = Field(
        alias="wind(%)",
        description="percentage of wind",
        ge=0, le=100
    )


class Payload(BaseModel):
    load: int = Field(
        description="Amount of energy to produce"
    )
    powerplants: list[Plant] = Field(
        description="Powerplants at disposal"
    )
    fuels: Fuels = Field(
        description="Fuel prices to produce the electricity"
    )
    method: str = Field(
        default="normal",
        description="Choose the method to process the production plan, `normal` has a precision of 0.1 MHw but does not"
                    "work everytime, while `recursive` mode has a precision of 1MWh and works everytime. If this field "
                    "is not defined, the method will be `normal`",
    )


class ProductionPlan(BaseModel):
    name: str = Field(
        description="Name of the plant"
    )
    power: float = Field(
        alias="p",
        description="Power production assigned to the plant"
    )


class ProductionPlanFailed(BaseModel):
    message: str = Field(
        description="Description of why the production plan could not be processed"
    )
