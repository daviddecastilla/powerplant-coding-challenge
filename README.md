# Power Plant Coding Challenge
- Author : David de Castilla
- Email : daviddecastilla@gmail.com

In this project, I am coding an implementation of this challenge : 
[Plant coding challenge](https://github.com/gem-spaas/powerplant-coding-challenge/tree/master)

## How to run

### Using docker
#### Install docker (if not already installed)
If you don't have docker installed on your computer yet, you can install it by 
following [these instructions](https://docs.docker.com/get-docker/). 

#### Build image
To run this project, you will need to first build the image with this command 
(while being in the project root directory) : 
```shell
docker build -t powerplant-coding-challenge:latest .
```

#### Run image
Then you can run the image with this command : 
```shell
docker run --rm -it -p 8888:8888 powerplant-coding-challenge
```

Then you should be able to access the API (from the computer the docker container is running on) : 
- [API Documention](127.0.0.1:8888/docs)
- [/productionplan endpoint](127.0.0.1:8888/productionplan)

### Without docker
This project uses [Poetry](https://python-poetry.org/) with Python 3.9

#### Install the dependencies
To install the dependencies, you just need to run :
```shell
poetry install
```

And then you can run this next command to launch the server : 
```shell
poetry run uvicorn main:app --port 8888 --host 0.0.0.0
```

And access it (from the computer the server is running on): 
- [API Documention](127.0.0.1:8888/docs)
- [/productionplan endpoint](127.0.0.1:8888/productionplan)

### Unit tests 
Some unit tests have been implemented using pytest, you can run them by first installing the dev
dependencies : 
```shell
poetry install --with dev
```
Then you can run the unit tests with the command : 
```shell
poetry run pytest test_production_planner
```

## API Specificity
There are two methods implemented to process the production plan. You can 
choose the method in the payload with the field `method` in the JSON. This field
can have one of the two values : 
- `normal` : A method that has a precision of 0.1MWh but does not work in every case 
(with payload2.json for example)
- `recursive` : A recursive method that works in every case but with a precision 
of 1MWh. This method can also be slow (for payload3 for example).

If the field is not present, the method will be `normal`.

 