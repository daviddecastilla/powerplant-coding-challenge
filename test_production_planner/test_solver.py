import json

from pathlib import Path

import pytest

from production_planner import solver
from production_planner.solver import process_plant_prices, find_best_price, process_plant_assignment_recursive
from production_planner.models import Plant, Fuels, Payload, PlantTypes


@pytest.mark.parametrize(
    "payload_example_path,expected_prices",
    [(Path('test_production_planner/test_solver/payload1.json'), [32.0, 32.0, 71.0, 125.0, 0, 0]),
     (Path('test_production_planner/test_solver/payload2.json'), [32.0, 32.0, 38.5, 250.0, 0, 0]),
     (Path('test_production_planner/test_solver/payload3.json'), [34.0, 34.0, 41.0, 150.0, 0, 0])]
)
def test_process_plant_prices(payload_example_path: str, expected_prices):
    # Arrange
    with open(payload_example_path, 'r') as payload_file:
        payload = Payload.model_validate(
            json.load(payload_file)
        )

    # Act
    prices = process_plant_prices(payload.powerplants, payload.fuels)

    # Assert
    assert len(prices) == len(expected_prices)
    assert prices == expected_prices


@pytest.mark.parametrize(
    "payload_example_path,expected_price",
    [(Path('test_production_planner/test_solver/payload1.json'), 11776),
     (Path('test_production_planner/test_solver/payload2.json'), 15360),
     (Path('test_production_planner/test_solver/payload3.json'), 27880)]
)
def test_find_best_price(payload_example_path: str, expected_price: float):
    # Arrange
    with open(payload_example_path, 'r') as payload_file:
        payload = Payload.model_validate(
            json.load(payload_file)
        )

    # As this part of code is duplicated with the code in the function process_plant_assignment_recursive
    #  we could create a new function for this part
    prices = process_plant_prices(payload.powerplants, payload.fuels)

    id_sorted = sorted(range(len(prices)), key=lambda k: prices[k])

    prices = [prices[i] for i in id_sorted]
    bounds = []
    for i in id_sorted:
        if payload.powerplants[i].type is PlantTypes.WIND_TURBINE:
            power = round(payload.powerplants[i].pmax * payload.fuels.wind_power / 100)
            bound = (power, power)
        else:
            bound = (payload.powerplants[i].pmin, payload.powerplants[i].pmax)

        bounds.append(bound)

    asked_load = payload.load
    price_to_beat = max(prices) * asked_load

    # Act
    plant_assignment, best_price = find_best_price(tuple(bounds), asked_load, tuple(prices), price_to_beat, 0)

    # Assert
    # Test that the best price is the actual best price
    assert best_price == expected_price
    # Test that the sum of assignments is the sum
    assert sum(plant_assignment) == asked_load
    # Test that every assignment is within the bounds
    for assignment, bound in zip(plant_assignment, bounds):
        assert assignment == 0 or (bound[0] <= assignment <= bound[1])


@pytest.mark.parametrize(
    "payload_example_path",
    [Path('test_production_planner/test_solver/payload1.json'),
     Path('test_production_planner/test_solver/payload2.json'),
     Path('test_production_planner/test_solver/payload3.json')]
)
def test_process_plant_assignment_recursive(payload_example_path: str):
    # Arrange
    with open(payload_example_path, 'r') as payload_file:
        payload = Payload.model_validate(
            json.load(payload_file)
        )

    prices = process_plant_prices(payload.powerplants, payload.fuels)

    # Act
    assignments = process_plant_assignment_recursive(
        prices, payload.powerplants, payload.load, payload.fuels
    )

    # Assert
    assert sum(assignments) == payload.load
    for assignment, plant in zip(assignments, payload.powerplants):
        if plant.type == PlantTypes.WIND_TURBINE:
            assert assignment == 0 or assignment == round(payload.fuels.wind_power * plant.pmax / 100)
        else:
            assert assignment == 0 or (plant.pmin <= assignment <= plant.pmax)


def test_solver_raise_value_error():
    with open('test_production_planner/test_solver/payload1.json', 'r') as payload_file:
        payload = Payload.model_validate(
            json.load(payload_file)
        )

    with pytest.raises(ValueError):
        solver(payload.load, payload.powerplants, payload.fuels, method="method_that_does_not_exist")
