from typing import Union

from fastapi import FastAPI
from fastapi.responses import JSONResponse

from production_planner.models import Payload, ProductionPlan, ProductionPlanFailed
from production_planner.exceptions import PowerAssignmentFailedError
from production_planner import solver


app = FastAPI()


@app.post("/productionplan",
          responses={
              '200': {'model': list[ProductionPlan]},
              '400': {'model': ProductionPlanFailed}
          })
def production_plan(payload: Payload) -> JSONResponse:
    """
    This method processes the repartition between power plants sent in the payload with the fuel with the goal
    of minimizing the electricity production price.

    Args:
        payload: Information about the asked load, the power plants available and fuel information.

    Returns:
        JSONResponse: The production if able to process it with status code 200,
            else it will be status code 400 with a message explaining the error

    """
    # Getting the assignment from our solver
    try:
        assignment = solver(payload.load, payload.powerplants, payload.fuels, payload.method)
    except PowerAssignmentFailedError:
        return JSONResponse(status_code=400, content=ProductionPlanFailed(
            message='Unable to process the production plan'
        ).dict())
    except ValueError:
        return JSONResponse(status_code=400, content=ProductionPlanFailed(
            message="Production plan method not supported"
        ).dict())

    # Package everything to send it back to the client
    plant_attribution = [
        ProductionPlan(
            name=plant.name,
            p=power,
        ).dict() for plant, power in zip(payload.powerplants, assignment)
    ]

    return JSONResponse(status_code=200, content=plant_attribution)
