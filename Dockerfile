FROM python:3.9-slim as builder

WORKDIR /app
RUN pip install poetry

COPY poetry.lock ./
COPY pyproject.toml ./

RUN poetry install --no-root

COPY main.py ./
COPY production_planner ./production_planner/

EXPOSE 8888/tcp
CMD poetry run uvicorn main:app --port 8888 --host 0.0.0.0

